Praktikum Sistem Operasi (A)
- Alif Adrian Anzary - 5025201274
- Ferdinand Putra Gumilang Silalahi - 5025201176
- Elbert Dicky Aristyo - 5025201231

# Revisi No. 1

# 1A
Encode dengan ROT 13 dihandling menggunakan fungsi encodeRot13

```
void encodeRot13(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, 0);

    for (int i = begin; i < end; i++){
        if (path[i] != '/' && isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i])) 
                continue; 
            else
                temp -= 'a';
            
            temp = (temp + 13) % 26;
            
            if(isupper(path[i]))
                continue;
            else temp += 'a';
            
            path[i] = temp;
        }
    }
}

```
# 1B
Untuk melakukan rename direktori, harus membaca direktori menggunakan fungsi xmp_readdir

```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

        if (dua != NULL) encodeVignere(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

```
Untuk melakukan rename file, harus membaca file menggunakan fungsi xmp_read

```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

```
Rename nya sendiri menggunakan fungsi xmp_rename
```
static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(to, ian);
    if (dua != NULL) decodeVignere(dua);

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}

```

# 1C
Decoding dilakukan menggunakan fungsi decodeRot13

```
void decodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
	
	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i]))
                continue;
            else temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i])) continue;
            else 
                temp += 'a';
			
            path[i] = temp;
		}
	}
}

```

# 1D
Pencatatan ke log menggunakan fungsi writeLog
```
void writeLog(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/andrianalif/modul4/Wibu.log", "a+");
    if(mode == 1)
        fprintf(fptr, "%s %s\n", str, from);
    else if(mode == 2)
        fprintf(fptr, "%s %s --> %s\n", str, from, to);
    fclose(fptr);
}

```
# 1E
Handling rekursi juga menggunakan fungsi xmp_readdir
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

        if (dua != NULL) encodeVignere(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```

# Revisi No. 2
